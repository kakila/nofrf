## Copyright (C) 2010-2014 - Juan Pablo Carbajal
##
## This progrm is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @deftypefn {Function File} {[@var{G}, @var{oF}, @var{Un}, @var{Yn}] =}  nofrf (@var{u},@var{a},@var{y},@var{Fs},@var{iF})
## @deftypefnx {Function File} {[@dots{}] =}  nofrf (@dots{},@var{N},@var{l})
## Computes the non-linear output frequency response functions (nofrf) from input-output data.
## The nofrf are the non-linear analogy of the frequency response for linear 
## systems. However, nofrf are input dependent and therefore not an intrinsic 
## property of the system.
##
## @strong{Inputs}
## @table @var
## @item u
## Normalized input signal (waveform in [-1,1]). It is a n-by-1 array.
## The algorithm assumes this is a band-limited waveform with zero power
## spectrum outside the input frequency range @var{iF}.
## @item a
## Array of amplitudes used as inputs. The actual input to the system
## is assumed to be @command{@var{u}.*@var{a}.'}. It is an k-by-1 array.
## @item y
## Output data. It is a n-by-m array where @command{@var{y}(:,j)} contains the 
## output signal corresponding to the input amplitude @command{@var{A}(j)}.
## @item Fs
## Sampling frequency.
## @item iF
## Input frequecy range. It is a 2-by-1 array indicating minimum and maximum
## frequencies in the spectrum of @var{u}.
## @item N
## Scalar indicating the maximum order of the NOFRF to be calculated (optional).
## This number cannot be higher than @command{length(@var{a})}(default value).
## @item l
## Scalar indicating regularization parameter (0.01 default value).
## @end table
##
## @strong{Outputs}
## @table @var
## @item G
## Nonlinear frequency response functions (NOFRFs). It is a complex k-by-N matrix, where
## each column is a NOFRF. The numer of rows is equal to @command{2^(nextpow2(n)+1)}.
## @item wY
## Output frequency ranges. A cell containing the support frequeny domains of each NOFRF.
## @item Un
## FFT of the input and its first @var{N} powers.
## @item Yn
## FFT of the outputs.
## @end table
##
## Note:
## The implementation follows the algorithm described in:
## Lang, Z. Q. & Billings, S. A. (2005). "Energy transfer properties of 
## non-linear systems in the frequency domain". International Journal of 
## Control, 78(5), 345-362. doi:10.1080/00207170500095759
## 
## However the normalization of the response functions in this algorithm
## doesn't follow the normalization of the article.
## @end deftypefn

function [G oF Un Yn] = nofrf (u,a,y,Fs,iF,N=[],l=1e-2)

  [np Na] = size (y);
  if length (a) != Na
    error ("Octave:invalid-input-arg", ...
           "The columns of Y should equal the length of A.\n");
  endif
  
  if isempty (N)
    N = Na;
  endif

  %% Frequency range
  % Frequency axis
  np = 2^nextpow2 (np);
  % Frequency vector, assuming uniform sampling.
  f = Fs * linspace (0,1,np/2+1);

  %% Input (and powers) Fourier transform
  Un = fft (u.^(1:N),np);

  %% Output Fourier transform
  Yn = fft (y,np);

  % amplitude matrix
  A    = kron (a.^(1:N), ones (2,1));
  patt = repmat ( (-1).^[1:2*Na]', 1, N);
  A = [A A.*patt];

  G = zeros (2*N, np);
  for i=1:np
    %% Build matrices
    % output matrix
    Y = zeros(2*Na,1);
    ind=1:2:(2*Na);
    Y(ind,1) = real(Yn(i,:));
    Y(ind+1,1) = imag(Yn(i,:));
    
    % input matrix
    U= repmat([real(Un(i,:)) imag(Un(i,:)); imag(Un(i,:)) real(Un(i,:))],Na,1);

    AU  = A .* U;
    %% NOFRF
    [u_,s_,v_] = svd (AU,true);
    % regularization
    s_ = diag(s_);
    f  = s_ ./ (s_.^2 + l.^2);
    G(:,i) = v_*(f.*(u_.'*Y));
    #    G(:,i) = pinv (AU)*Y;
    #    G(:,i) = AU \ Y;
    #    G(:,i) = xridgereg(AU,Y,logspace(-4,1,20),7);
  end

  G = complex( G(1:N,:), G((N+1):2*N,:) );

  %% Gn is zero out of the output frequency range
  [oF wY] = outputFrequencyRange97(iF, N);
  w = Fs/2 * linspace (0,1,np/2+1);
  for i = 1:N
    ind = false;
    for j=1:2:length(wY{i})
        ind = ind | (wY{i}(j)<= w & w<=wY{i}(j+1));
    end
    wind = find(~ind);
    G(i,wind) = complex(0,0);
    G(i,end-wind+1) = complex(0,0);
  end
  G = G.';

endfunction

%!demo
%! ## Dynamical system
%! w0 = 2*pi*5;
%! k  = w0^2*[1 0 1 0];
%! c  = 2*w0*0.06;
%! sys =@(s,x,p) [x(2); -polyval(k,x(1))-c*x(2) + p.A*p.input(s) ];
%!
%! ## Amplitudes to apply
%! nA  = 4;
%! a = linspace(100,800,nA).';
%!
%! ## Sampling frequency and related
%! nT = 2*1024; 
%! t  = linspace (0,5,nT).';
%! Fs = 1 / (t(2)-t(1));
%! 
%! ## Input signal
%! p.input =@(t) (1./(t+eps)).*(sin(55*2*pi*t)-sin(30*2*pi*t));
%! inorm   = 1 / (max (p.input (t)) - min (p.input (t)));
%! p.input =@(t) (inorm./(t+eps)).*(sin(15*2*pi*t)-sin(7*2*pi*t));
%! iF = [1 25]; 
%!
%! ## Generate output data
%! y  = zeros (nT,nA);
%! y0 = [0; 0];
%! for i=1:nA
%!    p.A    = a(i);
%!    y(:,i) = lsode (@(x,s)sys(s,x,p),y0,t)(:,1);
%! end
%! ## Plot data
%! figure (1, "name","Data");
%! clf;
%! subplot (2,1,1);
%! plot (t,p.input(t), "linewidth", 2);
%! ylabel('Input waveform');
%! set(gca,"xticklabel",[]);
%! axis tight
%! grid on
%! subplot (2,1,2)
%! plot (t,y, "linewidth", 2);
%! xlabel('Time')
%! ylabel('Output signal')
%! axis tight
%! grid
%!
%! ## Calculate NOFRF
%! [G oF] = nofrf (p.input(t),a,y,Fs,iF);
%! [n order] = size (G);
%! G = G(1:n/2+1,:);
%! f = Fs/2 * linspace (0, 1, size (G,1));
%!
%! ## Plot NOFRF
%! figure(2,"name","NOFRF")
%! clf
%! fmax = 0;
%! for i=1:order
%!   subplot(order,1,i);
%!   cla
%!   absG = abs (G(:,i));
%!   ind = absG > 0;
%!   absG(~ind)=NaN;
%!   semilogy(f,absG)
%!   axis tight
%!   ylabel(['G' num2str(i) '(f)'])
%!   grid on
%! 
%!   v = get(gca,'ylim')';
%!   fn = intervalUnion(oF{i});
%!   for j=1:size(fn,1)
%!       line(fn(j,1)*[1;1],v,'color','g')
%!       line(fn(j,2)*[1;1],v,'color','r')        
%!   end
%!  end  
%!  xlabel('Frequency [Hz]')
%! # -------------------------------------------------
%! # NOFRF of a Duffing oscillator
