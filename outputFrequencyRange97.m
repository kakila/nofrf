function [oRange fY]= outputFrequencyRange97(iRange,N)
%% oRange = outputFrequencyRange97(iRange,N)
% The function implements the algorithm in 
% Lang, Zi-Qiang and Billings, S. A.(1997) 'Output frequencies of nonlinear 
% systems', International Journal of Control, 67: 5, 713 -- 730
% http://dx.doi.org/10.1080/002071797223965
%
% INPUT
%    iRange: Input range of frequencies. If input is a general input is a 1x2 
%             row vector, with minium and maximum frequency in the input signal.
%             The algorithm is independt of the units used for the frequency.
%    N: The output frequency range is calculated up to the n-th order.
%    
% OUTPUT
%    oRange: A cell containig the frequency range for each order, i.e. oRange{j}
%            gives the frequency intervals for the j-th order. This is fYn in 
%            the 1997 publication.
%    fY: Gives the union of the real intervals in oRange.
%
% Example (as in the publication)
%!demo
%! % General input
%! iRange = [1 3]; % input signal with frequency band from 1 to 3 rad/s
%! disp('Output frequency range for a general input')
%! oRange = outputFrequencyRange97([0.5 0.7],3) % Output range up to 4th order
%!
%
% Copyright 2010 Juan Pablo Carbajal
% carbajal@ifi.uzh.ch
% Monday, September 20 2010
%
%    This file is part of the nofrf package.
%
%    The nofrf package is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    any later version.
%
%    The nofrf package is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with The nofrf package.  If not, see <http://www.gnu.org/licenses/>.

a = iRange(1); b=iRange(2);
ab = a+b;
alpha = a/ab;
beta = b/ab; 
oRange= cell(N,1);
fY_temp = oRange;
for n =1:N
    istar = ceil(n*alpha);
    k = [0:istar-1]';
    Ik = [n*a-k*ab n*b-k*ab; ...
                 0 n*b-istar*ab];
    oRange{n} = Ik;             
    if n*beta - floor(n*alpha) < 1                 
        oRange{n} = Ik(1:end-1,:);
    end
    fY{n} = sort(intervalUnion(oRange{n})'(:)');
end


        
