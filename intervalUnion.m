function u = intervalUnion(I)
%% u = intervalUnion(I)
% Union of the real intervals given in the rows of I.
%
% Example 
%!demo
%! % Intervals
%! I = [0 1; -.5 .5; .9 1.2; 3 6.2]; 
%! u = intervalUnion(I)
%
% Copyright 2010 Juan Pablo Carbajal
% carbajal@ifi.uzh.ch
% Thursday, September 23 2010
%
%    This file is part of the nofrf package.
%
%    The nofrf package is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    any later version.
%
%    The nofrf package is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with The nofrf package.  If not, see <http://www.gnu.org/licenses/>.

j=2;
k=1;
sw=0;
nI = size(I,1);       
if nI==1
 u=I;
 return
end
while true
    I1 = I(k,:);
    I2 = I(j,:);
    intersec = any([I1(1)<=I2(2) & I2(1)<=I1(1) , ...
                         I1(2)<=I2(2) & I2(1)<=I1(2) , ...
                         I2(1)<=I1(2) & I1(1)<=I2(1) , ...
                         I2(2)<=I1(2) & I1(1)<=I2(2) ]);
    if intersec
       I(k,:) = [ min([I1(1),I2(1)]) max([I1(2),I2(2)])];
       I(j,:) = [];
       nI = size(I,1);   
       sw =0;
    else
      I(j:end,:)=circshift(I(j:end,:),-1);
      sw = sw +1;
      if sw >= nI
        k=k+1;
        j=k+1;
        sw = 0;
      end
    end
    
    if j>nI 
      u=I;
      return
    end
end        

