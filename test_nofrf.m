% Copyright 2010 Juan Pablo Carbajal
% carbajal@ifi.uzh.ch
% Monday, September 20 2010
%
%    This file is part of the nofrf package.
%
%    The nofrf package is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    any later version.
%
%    The nofrf package is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with The nofrf package.  If not, see <http://www.gnu.org/licenses/>.

%% Script to reproduce plots from Z. Q. Langa; S. A. Billings 2005
clear all

%% Define the mechanical system
% stiffness polynomial
wL = 100;
k  = (wL^2)*[9e5 5e4 300 1 0];%
% Damping ratio
Q  = 0.06;
% Damping
c  = 2*Q*wL;

sys = @(s,x,p) [x(2); -polyval(k,x(1))-c*x(2) + p.A*p.func(s) ];

% Number and amplitudes to try
nA = 10;
a  = linspace(1,50,nA).';%[0.68 0.79 0.89 0.9].';

% Sampling frequency and related
T  = 0.0025;
Fs = 1/T;
N  = 2*1024;
t  = [-N/2+1:N/2]*T;
f = Fs/2*linspace(0,1,N/2+1);

%% Define the input signal
% First input of Reference of Z. Q. Langa; S. A. Billings 2005
iRange = [10; 35];
param.func = @(t) (1./(t+eps)).*(sin(iRange(2)*2*pi*t)-sin(iRange(1)*2*pi*t));
inorm = 1 / sqrt (quadgk(@(t)param.func(t).^2, floor(t(1)),ceil(t(end))));
param.func = @(t) (inorm./(t+eps)).*(sin(iRange(2)*2*pi*t)-sin(iRange(1)*2*pi*t));

%% Integrate the diff equations
%% Initialize solver    
x0 =[0; 0];
y  = zeros (length (t),nA);
opt =  odeset ("RelTol", 1e-6, "AbsTol", 1e-6, ...
           "InitialStep",T,...
           "NormControl", "on");
%% Solve
for i=1:nA
    param.A = a(i);
    %[tsol sol] = ode45 (@(s,x)sys(s,x,param),t,x0,opt);
    %y(:,i) = interp1(tsol,sol(:,1),t,'linear');
    sol     = lsode(@(x,s)sys(s,x,param),x0,t);
    y(:,i)  = sol(:,1);
end

%% Plot results
figure(1,"name","input-output data")
clf
subplot(2,1,1)
plot(t,param.func(t))
xlabel('t[sec]')
ylabel('Input waveform')
axis tight

subplot(2,1,2)
plot(t,y)
xlabel('t[sec]')
ylabel('Output signal')
axis tight

%% Calculate NOFRF
order = 4;
[G F U Y] = nofrf(param.func(t)',a,y,Fs,iRange,order);
nf = size(G,1);

%% Recover output from NOFRF
Yn = G.*U;
Yr = sum(Yn,2);
yr = ifft(Yr);

%% Plot FFTs
figure(2,"name","FFTs")
clf
subplot(2,1,1)
cla
semilogy (f, abs(U(1:nf/2+1,1)))
xlabel('Frequency [Hz]')
ylabel('||U*(f)||')
axis tight    
v = get(gca,'ylim');
line(F{1}(1,1)*[1;1],v,'color','g')
line(F{1}(1,2)*[1;1],v,'color','r')        

subplot(2,1,2)
cla
semilogy (f, abs(Y(1:nf/2+1,:)))
xlabel('Frequency [Hz]')
ylabel('||Yn(f)||')
axis tight    
v = get(gca,'ylim');
line(F{1}(1,1)*[1;1],v,'color','g')
line(F{1}(1,2)*[1;1],v,'color','r')        

%% Plot NOFRF
figure(3,"name","NOFRF")
clf
for i=1:order
    subplot(order,1,i)
    cla
    absG = abs(G(1:nf/2+1,i));
    ind = absG > 0;
    absG(~ind)=NaN;
    plot(f,log(absG))
    axis tight
    ylabel(['G' num2str(i) '(f)'])
    grid on

    v = get(gca,'ylim')';
    fn = intervalUnion(F{i});
    for j=1:size(fn,1)
        line(fn(j,1)*[1;1],v,'color','g')
        line(fn(j,2)*[1;1],v,'color','r')        
    end

%    nt=5;
%    tick = linspace(0,v(2),nt);
%    ticklabel{1}='0';
%    for j = 2:nt
%        ticklabel{j} = sprintf('%.1e ',tick(j));
%    end
%    set(gca,'ytick',tick,'yticklabel',ticklabel);
end
xlabel('Frequency [Hz]')
figure(4,"Name","Figure 5 of reference")
clf
for i=1:order
    subplot(order,1,i)
    cla
    ind = f>=10 & f<=20;
    plot(f(ind),abs(G(ind,i)))
    axis tight
    grid on

    v = get(gca,'ylim')';
    axis([10 20 0 v(2)]);
    nt=5;
    tick = linspace(0,v(2),nt);
    ticklabel{1}='0';
    for j = 2:nt
        ticklabel{j} = sprintf('%.1e ',tick(j));
    end
    set(gca,'ytick',tick,'yticklabel',ticklabel);
    ylabel(['G' num2str(i) '(f)'])

end
xlabel('Frequency [Hz]')

figure(5,"name","Recovered input FFT")
clf
for i=1:order
    subplot(order+1,1,i)
    cla
    semilogy (f,abs(Yn)(1:nf/2+1,i))
    axis tight
    ylabel(['Y' num2str(i) '(f)'])
    grid on

    v = get(gca,'ylim')';
    fn = intervalUnion(F{i});
    for j=1:size(fn,1)
        line(fn(j,1)*[1;1],v,'color','g')
        line(fn(j,2)*[1;1],v,'color','r')        
    end
end

subplot(order+1,1,i+1)
cla
semilogy (f,abs(Yr)(1:nf/2+1))
axis tight
ylabel(['Ytotal(f)'])
grid on
xlabel('Frequency [Hz]')
