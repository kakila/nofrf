Nonlinear Output Frequency Response Function (NOFRF)

The extension of the transfer function concept to nonlinear systems by meas of Volterra series. In collaboration with Dr. Zi-Qiang Lang from the university of Sheffield. 
Read the article: http://dx.doi.org/10.1016/j.jsv.2006.09.012
